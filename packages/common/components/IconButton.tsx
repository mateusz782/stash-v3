import React from "react";

export const IconButton: React.FC<IconButtonProps> = ({
  variant = "primary",
  elevated = false,
  onClick,
  loading,
  children,
}) => {
  return (
    <button
      type="submit"
      disabled={loading}
      onClick={onClick}
      className={`w-full px-1 py-1 inline-flex justify-center 
                  items-center text-center text-lg font-semibold 
                  text-white transition-colors duration-300 bg-${variant} 
                  rounded-md ${elevated && "shadow"} hover:${variant}-800 
                  focus:outline-none focus:ring-indigo-200 
                  focus:ring-2 disabled:opacity-50`}
    >
      {loading && (
        <svg
          className="animate-spin -ml-1 mr-3 h-5 w-5 text-white"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
        >
          <circle
            className="opacity-25"
            cx={12}
            cy={12}
            r={10}
            stroke="currentColor"
            strokeWidth={4}
          />
          <path
            className="opacity-75"
            fill="currentColor"
            d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
          />
        </svg>
      )}
      {children}
    </button>
  );
};

type IconButtonProps = {
  variant?: string;
  elevated?: boolean;
  onClick?: () => void;
  loading?: boolean;
};
