import React, { useState } from "react";
import { Dialog } from "@headlessui/react/dist/components/";

export const DialogConfirm: React.FC<{
  title: string;
  description: string;
}> = ({ title, description, children }) => {
  let [isOpen, setIsOpen] = useState(true);

  return (
    <Dialog open={isOpen} onClose={setIsOpen}>
      <Dialog.Overlay />

      <Dialog.Title>{title}</Dialog.Title>
      <Dialog.Description>{description}</Dialog.Description>

      <p>{children}</p>

      <button onClick={() => setIsOpen(false)}>Ok</button>
      <button onClick={() => setIsOpen(false)}>Cancel</button>
    </Dialog>
  );
};
