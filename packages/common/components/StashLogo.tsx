export const StashLogo = ({ width = 100, className }: LogoProps) => {
  return (
    <div style={{ textAlign: "center" }} className={className}>
      <img
        src="https://icongr.am/fontawesome/star.svg?size=31&color=ffae00"
        alt="logo"
        style={{ width, margin: "auto" }}
      />
    </div>
  );
};

interface LogoProps {
  width?: number;
  className?: string;
}
