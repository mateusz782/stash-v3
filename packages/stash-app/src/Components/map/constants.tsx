export const drawingOptions = [
  // {
  //   id: "move",
  //   name: "Navigate Map",
  //   description: "Move and pan map",
  //   icon: ({ color = "currentColor" }: { color?: string }) => (
  //     <svg
  //       xmlns="http://www.w3.org/2000/svg"
  //       version="1.1"
  //       viewBox="0 0 512 512"
  //       fill={color}
  //     >
  //       <path d="M372.57 112.641v-10.825c0-43.612-40.52-76.691-83.039-65.546-25.629-49.5-94.09-47.45-117.982.747C130.269 26.456 89.144 57.945 89.144 102v126.13c-19.953-7.427-43.308-5.068-62.083 8.871-29.355 21.796-35.794 63.333-14.55 93.153L132.48 498.569a32 32 0 0 0 26.062 13.432h222.897c14.904 0 27.835-10.289 31.182-24.813l30.184-130.958A203.637 203.637 0 0 0 448 310.564V179c0-40.62-35.523-71.992-75.43-66.359zm27.427 197.922c0 11.731-1.334 23.469-3.965 34.886L368.707 464h-201.92L51.591 302.303c-14.439-20.27 15.023-42.776 29.394-22.605l27.128 38.079c8.995 12.626 29.031 6.287 29.031-9.283V102c0-25.645 36.571-24.81 36.571.691V256c0 8.837 7.163 16 16 16h6.856c8.837 0 16-7.163 16-16V67c0-25.663 36.571-24.81 36.571.691V256c0 8.837 7.163 16 16 16h6.856c8.837 0 16-7.163 16-16V101.125c0-25.672 36.57-24.81 36.57.691V256c0 8.837 7.163 16 16 16h6.857c8.837 0 16-7.163 16-16v-76.309c0-26.242 36.57-25.64 36.57-.691v131.563z" />
  //     </svg>
  //   ),
  // },
  {
    id: "label",
    name: "Add Labels",
    placeholder: "i.e. Power Pole",
    description: "Mark your map with labels, i.e. POI, Landmarks, etc.",
    types: [
      "POI",
      "Landmark",
      "Power Pole",
      "Tree",
      "Sewer Discharge",
      "Retaining Wall",
    ],
    icon: ({ color = "currentColor" }: { color?: string }) => (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        viewBox="0 0 24 24"
        fill={color}
      >
        <path fill="none" d="M0 0h24v24H0z"></path>
        <path d="M12 20.9l4.95-4.95a7 7 0 1 0-9.9 0L12 20.9zm0 2.828l-6.364-6.364a9 9 0 1 1 12.728 0L12 23.728zM11 10V7h2v3h3v2h-3v3h-2v-3H8v-2h3z"></path>
      </svg>
    ),
  },
  {
    id: "polyline",
    name: "Draw Lines",
    placeholder: "i.e. road",
    description:
      "Draw single/multi lines, i.e. roads, distance, infrastructure, etc,",
    types: ["Distance", "Road", "Lane", "Sewer line"],
    icon: ({ color = "currentColor" }: { color?: string }) => (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        viewBox="0 0 24 24"
        fill={color}
      >
        <path
          xmlns="http://www.w3.org/2000/svg"
          d="M2 3V9H4.95L6.95 15H6V21H12V16.41L17.41 11H22V5H16V9.57L10.59 15H9.06L7.06 9H8V3M4 5H6V7H4M18 7H20V9H18M8 17H10V19H8Z"
        />
      </svg>
    ),
  },
  {
    id: "polygon",
    name: "Draw Area",
    placeholder: "i.e. Lot #301",
    description:
      "Draw polygons as areas, i.e. lots, stages, measurements, etc.",
    types: ["Area Measure", "Lot Boundary", "Development Stage"],
    icon: ({ color = "currentColor" }: { color?: string }) => (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        viewBox="0 0 24 24"
        fill={color}
      >
        <path
          xmlns="http://www.w3.org/2000/svg"
          d="M2,2V8H4.28L5.57,16H4V22H10V20.06L15,20.05V22H21V16H19.17L20,9H22V3H16V6.53L14.8,8H9.59L8,5.82V2M4,4H6V6H4M18,5H20V7H18M6.31,8H7.11L9,10.59V14H15V10.91L16.57,9H18L17.16,16H15V18.06H10V16H7.6M11,10H13V12H11M6,18H8V20H6M17,18H19V20H17"
        />
      </svg>
    ),
  },
  // {
  //   id: "delete",
  //   name: "Delete",
  //   description: "Delete shape or label",
  //   icon: ({ color = "currentColor" }: { color?: string }) => (
  //     <svg
  //       xmlns="http://www.w3.org/2000/svg"
  //       version="1.1"
  //       viewBox="0 0 24 24"
  //       fill={color}
  //     >
  //       <path fill="none" d="M0 0h24v24H0z" />
  //       <path d="M7 4V2h10v2h5v2h-2v15a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V6H2V4h5zM6 6v14h12V6H6zm3 3h2v8H9V9zm4 0h2v8h-2V9z" />
  //     </svg>
  //   ),
  // },
  {
    id: "edit",
    name: "Edit",
    description: "Edit or Delete shape or label",
    icon: ({ color = "currentColor" }: { color?: string }) => (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.1"
        viewBox="0 0 24 24"
        fill={color}
      >
        <path d="M5 19h1.414l9.314-9.314-1.414-1.414L5 17.586V19zm16 2H3v-4.243L16.435 3.322a1 1 0 0 1 1.414 0l2.829 2.829a1 1 0 0 1 0 1.414L9.243 19H21v2zM15.728 6.858l1.414 1.414 1.414-1.414-1.414-1.414-1.414 1.414z"></path>
      </svg>
    ),
  },
];
