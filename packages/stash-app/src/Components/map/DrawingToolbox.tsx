import { useEffect, useState } from "react";
import { drawingOptions } from "./constants";
import { IconButton } from "@stash/common/components";
import { IoMdClose } from "react-icons/io";

export const DrawingToolbox = ({
  value,
  onChange,
  onChangeAttributes,
}: DrawingToolboxProps) => {
  const [selectedOption, setSelectedOption] = useState<null | string>(
    value || "move"
  );
  const [drawingAttributes, setDrawingAttributes] = useState<DrawingAttributes>(
    {
      type: "",
      label: "",
      color: "#000",
      width: 1,
      opacity: 1,
    }
  );
  useEffect(() => {
    onChange && onChange(selectedOption);
  }, [selectedOption]);
  useEffect(() => {
    onChangeAttributes && onChangeAttributes(drawingAttributes);
  }, [drawingAttributes]);

  const { type } = drawingAttributes;
  const option = drawingOptions.find((item) => item.id === selectedOption);
  const defaultType = type || (option?.types && option.types[0]);
  return (
    <div className="z-1000 relative">
      <div>Drawing Attributes goes here...</div>
      <div
        style={{
          right: 10,
          top: 10,
          boxShadow: "0 0 0 2px rgb(0 0 0 / 10%)",
        }}
        className="bg-white rounded-sm p-0 absolute"
      >
        {drawingOptions.map((item, index) => {
          const isSelected = selectedOption === item.id;
          return (
            <button
              key={item.id}
              className={`w-7 h-7 block outline-none hover:bg-gray-100 focus:outline-none border-0 cursor-pointer ${
                index > 0 && "border-t"
              } border-gray-300 border-solid`}
              onClick={() => setSelectedOption(isSelected ? "move" : item.id)}
              title={item.name}
            >
              <div className="w-6 h-6 p-1 mx-auto">
                <item.icon color={isSelected ? "#ffae00" : "#000"} />
              </div>
            </button>
          );
        })}
      </div>
    </div>
  );
};

interface DrawingToolboxProps {
  value?: SelectedOption;
  onChange?: (SelectedOption) => void;
  onChangeAttributes?: (DrawingAttributes) => void;
}
interface DrawingAttributes {
  type: string;
  label: string;
  color: string;
  width: number;
  opacity: number;
}
type SelectedOption = "move" | "edit" | "polygon" | "polyline" | "delete";
interface State {
  selectedOption: SelectedOption;
  selectedType: string;
}
