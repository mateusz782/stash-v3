import React, { useState, useContext, useRef, useEffect } from "react";
import DeckGL from "@deck.gl/react";
import { MapController } from "@deck.gl/core";
import {
  ScaleControl,
  StaticMap,
  NavigationControl,
  _MapContext as MapContext,
} from "react-map-gl";
import {
  EditableGeoJsonLayer,
  SelectionLayer,
  EditMode,
  ModifyMode,
  ResizeCircleMode,
  TranslateMode,
  TransformMode,
  ScaleMode,
  RotateMode,
  DuplicateMode,
  ExtendLineStringMode,
  SplitPolygonMode,
  ExtrudeMode,
  ElevationMode,
  DrawPointMode,
  DrawLineStringMode,
  DrawPolygonMode,
  DrawRectangleMode,
  DrawCircleByDiameterMode,
  DrawCircleFromCenterMode,
  DrawEllipseByBoundingBoxMode,
  DrawEllipseUsingThreePointsMode,
  DrawRectangleUsingThreePointsMode,
  Draw90DegreePolygonMode,
  DrawPolygonByDraggingMode,
  MeasureDistanceMode,
  //   MeasureAreaMode,
  MeasureAngleMode,
  ViewMode,
  CompositeMode,
  SnappableMode,
  ElevatedEditHandleLayer,
  PathMarkerLayer,
  SELECTION_TYPE,
  GeoJsonEditMode,
} from "nebula.gl";
// import {
//   HtmlOverlay,
//   HtmlOverlayItem,
//   HtmlClusterOverlay,
// } from "@nebula.gl/overlays";
import { DrawingToolbox } from "./DrawingToolbox";

const MAPBOX_ACCESS_TOKEN =
  "pk.eyJ1IjoiZ2Vvcmdpb3MtdWJlciIsImEiOiJjanZidTZzczAwajMxNGVwOGZrd2E5NG90In0.gdsRu_UeU_uPi9IulBruXA";
const CompositeViewEditMode = new CompositeMode([
  new ModifyMode(),
  new ViewMode(),
]);
const modes = {
  move: ViewMode,
  polyline: DrawLineStringMode,
  polygon: DrawPolygonMode,
  edit: CompositeViewEditMode,
};
const controller = {
  doubleClickZoom: false,
};

export const Map = () => {
  const drawingAttrRef = useRef();
  const [state, setState] = useState({
    data: {
      type: "FeatureCollection",
      features: [],
    },
    selectedFeatureIndexes: [],
    mode: ViewMode,
  });
  const [viewState, setViewState] = useState({
    longitude: -122.4,
    latitude: 37.8,
    pitch: 0,
    bearing: 0,
    zoom: 12,
  });
  const { mode, selectedFeatureIndexes, data, editType } = state;
  const layers = [
    new EditableGeoJsonLayer({
      id: "geojson-layer",
      selectedFeatureIndexes,
      mode,
      data,
      onEdit: ({ updatedData, editType, editContext }) => {
        if (editType === "addFeature") {
          const newFeatureIndex = updatedData.features.length - 1;
          const newFeature = updatedData.features[newFeatureIndex];
          if (newFeature) {
            newFeature["properties"] = {
              ...newFeature["properties"],
              ...drawingAttrRef.current,
            };
          }
          setState({
            ...state,
            data: updatedData,
          });
        } else {
          setState({
            ...state,
            data: updatedData,
          });
        }
      },
    }),
  ];
  const changeMapMode = (selectedOption) => {
    const Mode = modes[selectedOption] || ViewMode;
    setState({ ...state, mode: Mode, selectedFeatureIndexes: [] });
  };
  const changeDrawingAttributes = (attributes) => {
    drawingAttrRef.current = attributes;
    console.info(attributes);
  };

  return (
    <div className="relative w-full h-full">
      <DrawingToolbox
        onChange={changeMapMode}
        onChangeAttributes={changeDrawingAttributes}
      />
      <DeckGL
        pickingRadius={10}
        viewState={viewState}
        initialViewState={{ pitch: 30, bearing: 0 }}
        width="100%"
        height="100%"
        onViewStateChange={(e) => setViewState(e.viewState)}
        controller={{ type: MapController, doubleClickZoom: false }}
        layers={layers}
        style={{ pointerEvents: "all" }}
        ContextProvider={MapContext.Provider}
        onClick={(info) => {
          if (mode === CompositeViewEditMode) {
            if (
              info &&
              info.index !== -1 &&
              info.index < data.features.length
            ) {
              let selectIndex = info.index;

              // avoid re-selecting if removing point on shape
              // featureIndex is the same as selected index and does not jump to other feature
              if (info.object?.properties.featureIndex !== undefined) {
                selectIndex = info.object?.properties.featureIndex;
              }

              setState({
                ...state,
                selectedFeatureIndexes: [selectIndex],
              });
            }
          }
        }}
      >
        <div
          className="absolute w-full h-full"
          style={{
            top: 0,
            right: 0,
          }}
        >
          <NavigationControl style={{ bottom: 36, right: 10 }} />
        </div>
        <StaticMap
          mapboxApiAccessToken={MAPBOX_ACCESS_TOKEN}
          // mapStyle="mapbox://styles/mapbox/satellite-v9"
        >
          <ScaleControl style={{ bottom: 36, left: 10 }} />
        </StaticMap>
      </DeckGL>
    </div>
  );
};

/* Troubleshooting:

Problem: index.js:1 deck: error during update of EditableGeoJsonLayer({id: 'geojson-layer'}) TypeError: Cannot read property 'geometry' of undefined
Solution: Deck will complain if entering into ModifyMode but no feature selected

*/
